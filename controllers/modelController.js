const Model = require("../models/Model");


let model = new Model("../db/posts.json");
let description = "";

class ModelController {

    constructor(pathJson, description) {
        model = new Model(pathJson);
        description = description;
    }

    getAllModels(req, res) {
        let models = model.getAllModels();
        res.json({
            message: "Get All " + description,
            data: models,
        });
    }

    getModelById(req, res) {
        let id = req.params.id;
        let modelOne = model.getModelById(id);
        res.json({
            message: "get " + description + " by id",
            data: modelOne,
        });
    }
}

module.exports = ModelController;
