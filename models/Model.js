class Model {
    constructor(pathJson) {
        this.Models = require(pathJson);
    }

    #getModels() {
        return this.Models;
    }

    getAllModels() {
        return this.#getModels();
    }

    getModelById(id) {
        const Models = this.#getModels();

        let modelsFiltered = Models.filter((model) => model.id == id);

        return modelsFiltered;
    }
}

module.exports = Model;
