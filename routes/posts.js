var express = require("express");
var router = express.Router();
const ModelController = require("../controllers/modelController");

const postController = new ModelController("../db/posts.json", "post");

/* GET post listing. */
router.get("/", postController.getAllModels);

/* GET psot by id. */
router.get("/:id", postController.getModelById);

module.exports = router;
