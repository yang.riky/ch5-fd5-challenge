var express = require("express");
var router = express.Router();
const ModelController = require("../controllers/modelController");

const userController = new ModelController("../db/users.json", "user");
/* GET users listing. */
router.get("/", userController.getAllModels);

router.get("/:id", userController.getModelById);

module.exports = router;
